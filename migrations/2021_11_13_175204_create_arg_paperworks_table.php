<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArgPaperworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arg_paperworks', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('part_id')->comment("PDF or JPG");
            $table->string('type');
            $table->string('paperwork_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arg_paperworks');
    }
}
