<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArgPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arg_parts', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('part_owner_id');
            $table->string('part_number');
            $table->string('serial_number');
            $table->text('description');
            $table->double('average_repair_cost');
            $table->string('alternative_part_number');
            $table->text('additional_notes')->nullable();
            $table->integer('part_info_id');
            $table->integer('part_category_id');
            $table->integer('part_condition_id');
            $table->integer('part_manufacturer_id');
            $table->integer('aircraft_info_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arg_parts');
    }
}
