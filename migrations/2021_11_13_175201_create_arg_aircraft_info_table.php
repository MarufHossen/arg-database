<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArgAircraftInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arg_aircraft_info', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('aircraft_model');
            $table->string('aircraft_tail_number');
            $table->text('aircraft_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arg_aircraft_info');
    }
}
