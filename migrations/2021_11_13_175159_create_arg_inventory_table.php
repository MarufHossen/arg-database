<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArgInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arg_inventory', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('item_number')->comment("Item number is not mandatory but can be used for new type of pattern like SKU");
            $table->integer('part_id');
            $table->integer('initial_quantity');
            $table->integer('current_quantity');
            $table->double('internal_price');
            $table->double('external_price');
            $table->integer('inventory_location_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arg_inventory');
    }
}
