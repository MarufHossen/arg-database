CREATE TABLE "arg_part_conditions"(
    "id" INTEGER NOT NULL,
    "condition_code" VARCHAR(255) NOT NULL,
    "condition_name" VARCHAR(255) NULL,
    "condition_description" TEXT NULL
);
ALTER TABLE
    "arg_part_conditions" ADD PRIMARY KEY("id");
CREATE TABLE "arg_inventory_locations"(
    "id" INTEGER NOT NULL,
    "location_code" VARCHAR(255) NOT NULL,
    "location_name" VARCHAR(255) NULL,
    "location_description" TEXT NULL
);
ALTER TABLE
    "arg_inventory_locations" ADD PRIMARY KEY("id");
CREATE TABLE "arg_part_photographs"(
    "id" INTEGER NOT NULL,
    "part_id" INTEGER NOT NULL,
    "photo_url" VARCHAR(255) NOT NULL
);
ALTER TABLE
    "arg_part_photographs" ADD PRIMARY KEY("id");
CREATE TABLE "arg_parts"(
    "id" INTEGER NOT NULL,
    "part_owner_id" INTEGER NOT NULL,
    "part_number" VARCHAR(255) NOT NULL,
    "serial_number" VARCHAR(255) NOT NULL,
    "description" TEXT NOT NULL,
    "average_repair_cost" DOUBLE PRECISION NOT NULL,
    "alternative_part_number" VARCHAR(255) NOT NULL,
    "additional_notes" TEXT NULL,
    "part_info_id" INTEGER NOT NULL,
    "part_category_id" INTEGER NOT NULL,
    "part_condition_id" INTEGER NOT NULL,
    "part_manufacturer_id" INTEGER NOT NULL,
    "aircraft_info_id" INTEGER NOT NULL
);
ALTER TABLE
    "arg_parts" ADD PRIMARY KEY("id");
CREATE TABLE "arg_inventory"(
    "id" INTEGER NOT NULL,
    "item_number" INTEGER NOT NULL,
    "part_id" INTEGER NOT NULL,
    "initial_quantity" INTEGER NOT NULL,
    "current_quantity" INTEGER NOT NULL,
    "internal_price" DOUBLE PRECISION NOT NULL,
    "external_price" DOUBLE PRECISION NOT NULL,
    "inventory_location_id" INTEGER NOT NULL
);
ALTER TABLE
    "arg_inventory" ADD PRIMARY KEY("id");
COMMENT
ON COLUMN
    "arg_inventory"."item_number" IS 'Item number is not mandatory but can be used for new type of pattern like SKU';
CREATE TABLE "arg_part_ownership"(
    "id" INTEGER NOT NULL,
    "name" INTEGER NOT NULL
);
ALTER TABLE
    "arg_part_ownership" ADD PRIMARY KEY("id");
CREATE TABLE "arg_aircraft_info"(
    "id" INTEGER NOT NULL,
    "aircraft_model" VARCHAR(255) NOT NULL,
    "aircraft_tail_number" VARCHAR(255) NOT NULL,
    "aircraft_description" TEXT NULL
);
ALTER TABLE
    "arg_aircraft_info" ADD PRIMARY KEY("id");
CREATE TABLE "arg_manufacturers_info"(
    "id" INTEGER NOT NULL,
    "manufaturer_name" VARCHAR(255) NOT NULL,
    "mfg_code" VARCHAR(255) NOT NULL
);
ALTER TABLE
    "arg_manufacturers_info" ADD PRIMARY KEY("id");
CREATE TABLE "arg_part_categories"(
    "id" INTEGER NOT NULL,
    "category_name" VARCHAR(255) NOT NULL,
    "category_description" TEXT NOT NULL
);
ALTER TABLE
    "arg_part_categories" ADD PRIMARY KEY("id");
CREATE TABLE "arg_paperworks"(
    "id" INTEGER NOT NULL,
    "part_id" INTEGER NOT NULL,
    "type" VARCHAR(255) NOT NULL,
    "paperwork_url" VARCHAR(255) NOT NULL
);
ALTER TABLE
    "arg_paperworks" ADD PRIMARY KEY("id");
COMMENT
ON COLUMN
    "arg_paperworks"."part_id" IS 'PDF or JPG';
CREATE TABLE "arg_research_documents"(
    "id" INTEGER NOT NULL,
    "part_id" VARCHAR(255) NOT NULL,
    "type" VARCHAR(255) NOT NULL,
    "docs_url" VARCHAR(255) NOT NULL
);
ALTER TABLE
    "arg_research_documents" ADD PRIMARY KEY("id");
CREATE TABLE "arg_part_info"(
    "id" INTEGER NOT NULL,
    "hours" DOUBLE PRECISION NOT NULL,
    "life_remaining" VARCHAR(255) NOT NULL,
    "cycles" BIGINT NOT NULL
);
ALTER TABLE
    "arg_part_info" ADD PRIMARY KEY("id");
ALTER TABLE
    "arg_parts" ADD CONSTRAINT "arg_parts_part_condition_id_foreign" FOREIGN KEY("part_condition_id") REFERENCES "arg_part_conditions"("id");
ALTER TABLE
    "arg_parts" ADD CONSTRAINT "arg_parts_part_owner_id_foreign" FOREIGN KEY("part_owner_id") REFERENCES "arg_part_ownership"("id");
ALTER TABLE
    "arg_parts" ADD CONSTRAINT "arg_parts_part_category_id_foreign" FOREIGN KEY("part_category_id") REFERENCES "arg_part_categories"("id");
ALTER TABLE
    "arg_parts" ADD CONSTRAINT "arg_parts_part_manufacturer_id_foreign" FOREIGN KEY("part_manufacturer_id") REFERENCES "arg_manufacturers_info"("id");
ALTER TABLE
    "arg_parts" ADD CONSTRAINT "arg_parts_aircraft_info_id_foreign" FOREIGN KEY("aircraft_info_id") REFERENCES "arg_aircraft_info"("id");
ALTER TABLE
    "arg_inventory" ADD CONSTRAINT "arg_inventory_part_id_foreign" FOREIGN KEY("part_id") REFERENCES "arg_parts"("id");
ALTER TABLE
    "arg_inventory" ADD CONSTRAINT "arg_inventory_inventory_location_id_foreign" FOREIGN KEY("inventory_location_id") REFERENCES "arg_inventory_locations"("id");
ALTER TABLE
    "arg_part_photographs" ADD CONSTRAINT "arg_part_photographs_part_id_foreign" FOREIGN KEY("part_id") REFERENCES "arg_parts"("id");
ALTER TABLE
    "arg_paperworks" ADD CONSTRAINT "arg_paperworks_part_id_foreign" FOREIGN KEY("part_id") REFERENCES "arg_parts"("id");
ALTER TABLE
    "arg_research_documents" ADD CONSTRAINT "arg_research_documents_part_id_foreign" FOREIGN KEY("part_id") REFERENCES "arg_parts"("id");
ALTER TABLE
    "arg_parts" ADD CONSTRAINT "arg_parts_part_info_id_foreign" FOREIGN KEY("part_info_id") REFERENCES "arg_part_info"("id");